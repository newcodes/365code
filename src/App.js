import logo from './logo.svg';
import './App.css';
// import MemberRegistration from './Registration/MemberRegistration';
import { HashRouter } from 'react-router-dom';
// import Plans from './plans/Plan';
import Navbar from './Navbar';
function App() {
  return (
    <>
     <HashRouter>
     <Navbar/>
     </HashRouter>

    </>
  );
}

export default App;
