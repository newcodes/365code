import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import CssBaseline from '@mui/material/CssBaseline';
import useScrollTrigger from '@mui/material/useScrollTrigger';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Grid from '@mui/material/Grid';
import { NavLink, Route } from 'react-router-dom';
import MemberRegistration from './Registration/MemberRegistration';
import Plans from './plans/Plan';


function ElevationScroll(props) {
  const { children, window } = props;
  // Note that you normally won't need to set the window ref as useScrollTrigger
  // will default to window.
  // This is only being set here because the demo is in an iframe.
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 0,
    target: window ? window() : undefined,
  });

  return React.cloneElement(children, {
    elevation: trigger ? 4 : 0,
  });
}

ElevationScroll.propTypes = {
  children: PropTypes.element.isRequired,
  /**
   * Injected by the documentation to work in an iframe.
   * You won't need it on your project.
   */
  window: PropTypes.func,
};

export default function Navbar(props) {
  return (
    <React.Fragment>
      <CssBaseline />
      <ElevationScroll {...props}>
        <AppBar>
          <Toolbar>
		  <Grid   >Home</Grid>
                    <Grid item sm>
						
					</Grid>
					<Grid item>
					<NavLink to="/Register" className="inactive" activeClassName="active" exact={true}>Register</NavLink>
					</Grid>
					<Grid><NavLink to="/Login" className="inactive" activeClassName="active" exact={true}>/Login</NavLink></Grid>
					<NavLink to="/plan" className="inactive" activeClassName="active" exact={true}>/Plan</NavLink>
          </Toolbar>
        </AppBar>
      </ElevationScroll>
      <Toolbar />
      <Container>
        
      </Container>
	  <Route path="/Register" component={MemberRegistration}></Route>
	  <Route path="/Plan" component={Plans}></Route>
    </React.Fragment>


  );
}
