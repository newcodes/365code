import * as React from "react";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Grid from '@mui/material/Grid';

export default function PlanDrop() {
  const [Plan, setPlan] = React.useState("");

  const handleChange = (event) => {
    setPlan(event.target.value);
  };

  return (
    <div>
      <FormControl sx={{ m: 1, minWidth: 500 }}>
        <Grid >
        <InputLabel id="demo-simple-select-autowidth-label">Plan</InputLabel>
        <Select
          labelId="demo-simple-select-autowidth-label"
          id="demo-simple-select-autowidth"
          value={Plan}
          onChange={handleChange}
          autoWidth
          label="plan"
        >
          <MenuItem value="">
            <em>None</em>
          </MenuItem>
          <MenuItem value={10}>356 Plan</MenuItem>
        </Select>
        </Grid>
      </FormControl>
    </div>
  );
}
