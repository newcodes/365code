import React  from "react";

import "./Plans.scss";

const Plans = () => {

    return (


      <div>


<section class="w-100 sec-plans">
  <div class="container-fluid shadow-lg mb-5">
    <div class="container">
      <div class="row plans-fluid-row pt-5">
        <div class="col-sm-12 gx-5 d-flex justify-content-between align-items-center launch-title mt-5 py-5">
          <div class="col-md-8">
            <h2 class="mb-1 fw-bold lh-lg">Get started today  Get 50% OFF</h2>
            {/* <h5 class="text-muted">Join us to aim at the moonshots!</h5> */}
            <h5 class="text-muted lh-lg fw-light">Sed vel accumsan lorem. Etiam ultrices in dui et dapibus. Maecenas tincidunt finibus ex. Nulla non lacus at mauris sodales consectetur.</h5>
          </div>
          <div class="col-md-4">
            <img src="/assets/img/plans.png" width="250px" class="img-fluid"/>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

{/* Pricing */}
<section class="w-100 sec-pricing" id="pricing">
  <div class="container">
    <div class="row pricing-row pt-5 pb-3 text-center">
      <h1 class="display-6 mb-4">
        Plans that satisfy everyone's needs
      </h1>
      {/* <p class="position-relative lh-lg fw-bold">Plans that meets everyone's needs</p> */}
    </div>
    <div class="row cust-nav">
      {/* Nav Pills */}
      <ul class="nav nav-pills rounded-pill justify-content-center mb-3 p-2 mx-auto" id="pills-tab" role="tablist" style= {{ }}>
        <li class="nav-item" role="presentation">
          <button class="nav-link btn-lg py-3 px-5 active" id="pills-monthly-tab" data-bs-toggle="pill" data-bs-target="#pills-monthly" type="button" role="tab" aria-controls="pills-monthly" aria-selected="true">Monthly</button>
        </li>
        <li class="nav-item" role="presentation">
          <button class="nav-link btn-lg py-3 px-5" id="pills-annually-tab" data-bs-toggle="pill" data-bs-target="#pills-annually" type="button" role="tab" aria-controls="pills-annually" aria-selected="false">Annually</button>
        </li>
      </ul>
      {/* Nav Pills Tab Content */}
      <div class="tab-content mt-2 mb-2" id="pills-tabContent">
        {/* Monthly Tab Content */}
        <div class="tab-pane fade show active" id="pills-monthly" role="tabpanel" aria-labelledby="pills-monthly-tab">
          <div id="pricingMontlyControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner p-5">
              <div class="carousel-item active">
                <div class="row row-cols-1 row-cols-md-3 g-5 sec-pricing-cards text-center">
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem Startup</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>400
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4 active">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem 30</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>475
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem 50</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>650
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>  
              <div class="carousel-item">
                <div class="row row-cols-1 row-cols-md-3 g-5 sec-pricing-cards text-center">
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem Premium</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>775
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4 active">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem Speedz</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>850
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem Extream</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>1099
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="row row-cols-1 row-cols-md-3 g-5 sec-pricing-cards text-center">
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem U 30</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>850
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4 active">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem U 50</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>1050
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem Ultra</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>1500
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#pricingMontlyControls" data-bs-slide="prev">
              <i class="fa fa-angle-left fa-3x" aria-hidden="true"></i>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#pricingMontlyControls" data-bs-slide="next">
              <i class="fa fa-angle-right fa-3x" aria-hidden="true"></i>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
        </div>
        {/* Annual Tab Content */}
        <div class="tab-pane fade" id="pills-annually" role="tabpanel" aria-labelledby="pills-annually-tab">
          <div id="pricingAnnuallyControls" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-inner p-5">
              <div class="carousel-item active">
                <div class="row row-cols-1 row-cols-md-3 g-5 sec-pricing-cards text-center">
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem Startup</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>4800
                          <sub class="d-block fs-6 badge rounded-pills">Extra 3 Months Free</sub>
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4 active">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem 30</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>5700
                          <sub class="d-block fs-6 badge rounded-pills">Extra 3 Months Free</sub>
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem 50</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>7800
                          <sub class="d-block fs-6 badge rounded-pills">Extra 3 Months Free</sub>
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="row row-cols-1 row-cols-md-3 g-5 sec-pricing-cards text-center">
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem Premium</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>9300
                          <sub class="d-block fs-6 badge rounded-pills">Extra 3 Months Free</sub>
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4 active">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem Speedz</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>10200
                          <sub class="d-block fs-6 badge rounded-pills">Extra 3 Months Free</sub>
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                      <div class="card-body">
                        <p class="card-paln fs-6 fw-bold">Bheem Extream</p>
                        <h1 class="card-title display-2 mb-3">
                          <sup class="fs-4 fw-light"><i class="fa fa-rupee"></i></sup>13188
                          <sub class="d-block fs-6 badge rounded-pills">Extra 3 Months Free</sub>
                        </h1>
                        {/* <p class="card-text fs-5">Maecenas sapien augue, elementum et facilisis et, consectetur nec est. Nunc sollicitudin, odio ut pharetra posuere sapien.</p> */}
                      </div>
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="row row-cols-1 row-cols-md-3 g-5 sec-pricing-cards text-center">
                 
                  
                  <div class="col">
                    <div class="card border-0 h-100 py-5 px-4">
                     
                      <div class="card-footer">
                        <button class="btn btn-c-secondary rounded-pill px-5 py-3 mt-3" type="button">Get Started</button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#pricingAnnuallyControls" data-bs-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#pricingAnnuallyControls" data-bs-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
        </div>
      </div>
    </div>

    {/* VIEW ALL PLANS */}
    {/*  */}
  </div>
</section>





        </div>

    );
}


export default Plans;
